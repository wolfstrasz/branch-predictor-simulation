# Branch Predictor Simulation

The project contains implementations of three types of branch prediction (all written in single file all_branch_predictors.cpp :
* Local History Branch Predictor
* Global History Branch Predictor
* Tournament Branch Predictor (hybrid of other two)

All predictors have been tested on three benchmarks from the SPEC2000 benchmark suite:
* Gromacs
* Gobmk
* Sjeng

## Report

The report explaining the implementation, analysis and testing done can be found as 'report.pdf'.

### Simulation handler

The branch predictors testing was executed on INTEL PIN TOOL:
pin-3.5-97503-gac534ca30-gcc-linux


## Authors

* **Boyan Yotov** - *Full work* - [wolfstrasz](https://gitlab.com/wolfstrasz)

