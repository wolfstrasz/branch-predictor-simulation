#include <iostream>
#include <fstream>
#include <cstdlib>
#include <math.h>
#include "pin.H"

// Simulation will be stop once this number of instructions is executed
#define STOP_INSTR_NUM 1000000000 //1b instrs
// Simulator heartbeat rate
#define SIMULATOR_HEARTBEAT_INSTR_NUM 100000000 //100m instrs

// Updates a chosen pattern history table
void updatePatternHistoryTable(UINT64 &pht, bool str);

// Updates a chosen history register
void updateHistoryRegister(UINT64 &historyRegister, bool isTaken, UINT64 numberOfEntries);



class BranchPredictorInterface {
public:
  //This function returns a prediction for a branch instruction with address branchIP
  virtual bool getPrediction(ADDRINT branchIP) = 0;
  //This function updates branch predictor's history with outcome of branch instruction with address branchIP
  virtual void train(ADDRINT branchIP, bool correctBranchDirection) = 0;
};

//#################################################################
//-------------- ALWAYS TAKEN BRANCH PREDICTOR --------------------
//#################################################################

class AlwaysTakenBranchPredictor : public BranchPredictorInterface {
public:
  AlwaysTakenBranchPredictor(UINT64 numberOfEntries) {}; //no entries here: always taken branch predictor is the simplest predictor
	virtual bool getPrediction(ADDRINT branchIP) {
		return true; // predict taken
	}
	virtual void train(ADDRINT branchIP, bool correctBranchDirection) {} //nothing to do here: always taken branch predictor does not have history
};

//#################################################################
//----------------- LOCAL BRANCH PREDICTOR ------------------------
//#################################################################

class LocalBranchPredictor : public BranchPredictorInterface {

// Variables defining the Local Branch Predictor
UINT64* patternHistoryTable;
UINT64* localHistoryRegisters;
UINT64  numberOfEntries;

// Variables filled by getPrediction(), used in train()
UINT64 LSBs;
UINT64* currentLHR;

public:
    LocalBranchPredictor(UINT64 numberOfEntries) {
        this->numberOfEntries = numberOfEntries;

		patternHistoryTable = new UINT64[numberOfEntries]();                    // Create the PHT,
		memset(patternHistoryTable, 3, numberOfEntries);                        // initialize to '0..011'

        localHistoryRegisters = new UINT64[128]();                              // Create 128 LHRs,
        memset(localHistoryRegisters, 0, 128);                                  // initialize to '0..000'
    };

    virtual bool getPrediction(ADDRINT branchIP) {
        LSBs = branchIP % 128;                                                  // Get the 7 LSBs
        currentLHR = &localHistoryRegisters[LSBs];                              // Get the history register
        return (patternHistoryTable [*currentLHR] >> 1) % 2 ;                   // Prediction returned by the
    }                                                                           //   MSB of the 2-bit counter

    virtual void train(ADDRINT branchIP, bool correctBranchDirection) {
        bool prediction = getPrediction(branchIP);                              // Obtain prediction
        bool isCorrect = (prediction == correctBranchDirection);                // Check correctness

        updatePatternHistoryTable(patternHistoryTable[*currentLHR], isCorrect);
        updateHistoryRegister(*currentLHR, correctBranchDirection, numberOfEntries);
    }
};

//#################################################################
//----------------- GSHARE BRANCH PREDICTOR -----------------------
//#################################################################

class GshareBranchPredictor : public BranchPredictorInterface {

    // Variables defining the Gshare Branch Predictor
    UINT64* globalHistoryRegister;
    UINT64* patternHistoryTable;
    UINT64  numberOfEntries;

    // Variables filled by getPrediction(), used in train()
    UINT64 addrXOR;

public:
    GshareBranchPredictor(UINT64 numberOfEntries) {
        this->numberOfEntries = numberOfEntries;

        patternHistoryTable = new UINT64[numberOfEntries]();                    // Create the PHT,
		memset(patternHistoryTable, 3, numberOfEntries);                        // initialize to '0..011'

        globalHistoryRegister = new UINT64;                                     // Create the GHR,
        memset(globalHistoryRegister,0,1);                                      // initialize to '0..000'

    }

    virtual bool getPrediction(ADDRINT branchIP) {
        addrXOR = (branchIP % numberOfEntries)                                  // Get the XOR of the log2(numberOfEntries)
                        ^ (*globalHistoryRegister % numberOfEntries);           //   bits of branch address with the GHR
        return ((patternHistoryTable[addrXOR]>>1)%2);                           // Prediction returned by the
    }                                                                           //   MSB of the 2-bit counter

    virtual void train(ADDRINT branchIP, bool correctBranchDirection) {
        bool prediction = getPrediction(branchIP);                              // Obtain prediction
        bool isCorrect = (prediction == correctBranchDirection);                // Check correctness

        updatePatternHistoryTable(patternHistoryTable[addrXOR], isCorrect);
        updateHistoryRegister(*globalHistoryRegister,correctBranchDirection,numberOfEntries);
    }
};

//#################################################################
//---------------- TOURNAMENT BRANCH PREDICTOR --------------------
//#################################################################

class TournamentBranchPredictor : public BranchPredictorInterface {

    // Variables defining the Gshare Branch Predictor
    LocalBranchPredictor* localBP;
    GshareBranchPredictor* gshareBP;
    UINT64 numberOfEntries;
    UINT64* patternHistoryTable;

    // Variables filled by getPrediction(), used in train()
    UINT64 LSBs;

public:
    TournamentBranchPredictor(UINT64 numberOfEntries) {

        this->numberOfEntries = numberOfEntries;

        patternHistoryTable = new UINT64[numberOfEntries];                      // Create the PHT,
        memset(patternHistoryTable, 3, numberOfEntries);                        // initialize to '11'

        localBP  = new LocalBranchPredictor(numberOfEntries);                   // Create predictors
        gshareBP = new GshareBranchPredictor(numberOfEntries);
    };

    virtual bool getPrediction(ADDRINT branchIP) {
        LSBs = branchIP % numberOfEntries;                                      // Get log2(numberOfEntries)
        return ((patternHistoryTable[LSBs]>>1)%2) ?                             // LSBs from the branch address
                    gshareBP->getPrediction(branchIP) :                         // use them to index PHT and
                        localBP->getPrediction(branchIP);                       // choose predictor to use
    }

    virtual void train(ADDRINT branchIP, bool correctBranchDirection) {

        bool prediction = getPrediction(branchIP);                              // Obtain prediction
        bool chosenPredictor = (patternHistoryTable[LSBs]>>1)%2;                // Obtain predictor chosen
        bool altPrediction = (chosenPredictor) ?                                // Obtain prediction of other predictor
                localBP->getPrediction(branchIP) :
                    gshareBP->getPrediction(branchIP);

        if(prediction == correctBranchDirection){                               // Correct choice -> strengthen
            updatePatternHistoryTable(patternHistoryTable[LSBs],true);          // Wrong and other choice was correct -> weaken
        } else if (altPrediction == correctBranchDirection){                    // Both choices wrong -> do nothing
            updatePatternHistoryTable(patternHistoryTable[LSBs],false);
        } else {
            // DO NOTHING
        }

        gshareBP->train(branchIP,correctBranchDirection);                       // Total Update Policy
        localBP->train(branchIP,correctBranchDirection);                        // => train both predictors

    }
};



//#################################################################
//------------------------ SIMULATIONS ----------------------------
//#################################################################

ofstream OutFile;
BranchPredictorInterface *branchPredictor;

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "BP_stats.out", "specify output file name");
KNOB<UINT64> KnobNumberOfEntriesInBranchPredictor(KNOB_MODE_WRITEONCE, "pintool",
    "num_BP_entries", "1024", "specify number of entries in a branch predictor");
KNOB<string> KnobBranchPredictorType(KNOB_MODE_WRITEONCE, "pintool",
    "BP_type", "always_taken", "specify type of branch predictor to be used");

// The running counts of branches, predictions and instructions are kept here
static UINT64 iCount                          = 0;
static UINT64 correctPredictionCount          = 0;
static UINT64 conditionalBranchesCount        = 0;
static UINT64 takenBranchesCount              = 0;
static UINT64 notTakenBranchesCount           = 0;
static UINT64 predictedTakenBranchesCount     = 0;
static UINT64 predictedNotTakenBranchesCount  = 0;

// Increments counter of instructions
VOID docount();

// Stops the simulation handler
VOID TerminateSimulationHandler(VOID *v);
VOID Fini(int code, VOID * v);

// This function is called before every conditional branch is executed
static VOID AtConditionalBranch(ADDRINT branchIP, BOOL correctBranchDirection);

// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v);

// Print Help Message
INT32 Usage();



/*
* NOTE:
*  KnobNumberOfEntriesInBranchPredictor.Value() returns the value specified after tool option "-num_BP_entries"
*    Argument of tool option "-num_BP_entries" is an integer
*  KnobBranchPredictorType.Value() returns the value specified after tool option "-BP_type"
*    Argument of tool option "-BP_type" can be one of the followong strings: "always_taken", "local", "gshare", "tournament"
*/
int main(int argc, char * argv[]) {
  // Initialize pin
  if (PIN_Init(argc, argv)) return Usage();

  // Create a branch predictor object of requested type
  if (KnobBranchPredictorType.Value() == "always_taken") {
    std::cerr << "Using always taken BP" << std::endl;
    branchPredictor = new AlwaysTakenBranchPredictor(KnobNumberOfEntriesInBranchPredictor.Value());
  }
  else if (KnobBranchPredictorType.Value() == "local") {
  	 std::cerr << "Using Local BP." << std::endl;
     branchPredictor = new LocalBranchPredictor(KnobNumberOfEntriesInBranchPredictor.Value());
  }
  else if (KnobBranchPredictorType.Value() == "gshare") {
  	 std::cerr << "Using Gshare BP."<< std::endl;
     branchPredictor = new GshareBranchPredictor(KnobNumberOfEntriesInBranchPredictor.Value());
  }
  else if (KnobBranchPredictorType.Value() == "tournament") {
  	 std::cerr << "Using Tournament BP." << std::endl;
     branchPredictor = new TournamentBranchPredictor(KnobNumberOfEntriesInBranchPredictor.Value());
  }
  else {
    std::cerr << "Error: No such type of branch predictor. Simulation will be terminated." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  std::cerr << "The simulation will run " << STOP_INSTR_NUM << " instructions." << std::endl;

  OutFile.open(KnobOutputFile.Value().c_str());

  // Register Instruction to be called to instrument instructions
  INS_AddInstrumentFunction(Instruction, 0);

  // Function to be called if the porgram finishes before it completes 10b instructions
  PIN_AddFiniFunction(Fini, 0);

  // Callback functions to invoke before Pin releases control of the application
  PIN_AddDetachFunction(TerminateSimulationHandler, 0);

  // Start the program, never returns
  PIN_StartProgram();

  return 0;
}

//#################################################################
//------------------- FUNCTION DEFINITIONS ------------------------
//#################################################################

// Increments counter of instructions
VOID docount() {
	// Update instruction counter
	iCount++;
	// Print this message every SIMULATOR_HEARTBEAT_INSTR_NUM executed
	if (iCount % SIMULATOR_HEARTBEAT_INSTR_NUM == 0) {
		std::cerr << "Executed " << iCount << " instructions." << endl;
	}
	// Release control of application if STOP_INSTR_NUM instructions have been executed
	if (iCount == STOP_INSTR_NUM) {
		PIN_Detach();
	}
}

// Stops the simulation handler
VOID TerminateSimulationHandler(VOID *v) {
	OutFile.setf(ios::showbase);
	// At the end of a simulation, print counters to a file
	OutFile << "Prediction accuracy:\t" << (double)correctPredictionCount / (double)conditionalBranchesCount << endl
		<< "Number of conditional branches:\t" << conditionalBranchesCount << endl
		<< "Number of correct predictions:\t" << correctPredictionCount << endl
		<< "Number of taken branches:\t" << takenBranchesCount << endl
		<< "Number of non-taken branches:\t" << notTakenBranchesCount << endl
		;
	OutFile.close();

	std::cerr << endl << "PIN has been detached at iCount = " << STOP_INSTR_NUM << endl;
	std::cerr << endl << "Simulation has reached its target point. Terminate simulation." << endl;
	std::cerr << "Prediction accuracy:\t" << (double)correctPredictionCount / (double)conditionalBranchesCount << endl;
	std::exit(EXIT_SUCCESS);
}
VOID Fini(int code, VOID * v)
{
	TerminateSimulationHandler(v);
}

// This function is called before every conditional branch is executed
static VOID AtConditionalBranch(ADDRINT branchIP, BOOL correctBranchDirection) {
	/*
	* This is the place where predictor is queried for a prediction and trained
	*/

	bool prediction = branchPredictor->getPrediction(branchIP);
	branchPredictor->train(branchIP, correctBranchDirection);

	// Update stat counters
	if (prediction) {
		predictedTakenBranchesCount++;
	}
	else {
		predictedNotTakenBranchesCount++;
	}
	if (correctBranchDirection) {
		takenBranchesCount++;
	}
	else {
		notTakenBranchesCount++;
	}
	if (prediction == correctBranchDirection) correctPredictionCount++;
	conditionalBranchesCount++;
}

// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v) {
	//Inser a call before every instruction to count them
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)docount, IARG_END);

	// Insert a call before every conditional branch
	if (INS_IsBranch(ins) && INS_HasFallThrough(ins)) {
		INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)AtConditionalBranch, IARG_INST_PTR, IARG_BRANCH_TAKEN, IARG_END);
	}
}

// Print Help Message
INT32 Usage() {
	cerr << "This tool simulates different types of branch predictors" << endl;
	cerr << endl << KNOB_BASE::StringKnobSummary() << endl;
	return -1;
}

// Updates a chosen history register
void updateHistoryRegister(UINT64 &historyRegister, bool isTaken, UINT64 numberOfEntries) {
	historyRegister = ((historyRegister << 1) + isTaken) % numberOfEntries;
}

// Updates a chosen pattern history table
void updatePatternHistoryTable(UINT64 &pht, bool str) {
	bool msb = (pht >> 1) % 2;                                  // MSB = predictor bit of counter
	bool lsb = (pht % 2);                                       // LSB = strength of predictor bit
	pht = (((!str && lsb) || (msb && str)) << 1)                // Boolean function derivation for the MSB
		+ ((msb && str) || (!lsb && !str));						// Boolean function derivation for the LSB
}






